# Rest Practice Backend



## Getting started


Hi and welcome, this repository contains the source code for a php-based web app that shows how to consume and create
a modern and quick web app from a REST service from IDBM.

Available online at (Coming Soon)


Code is organized as follows:


A model file responsible for management all DB conections and queries.


1. An API folder containing all the queries to the IDMB rest api endpoints. It acts like a "model"


2. A folder name config containing a file for configuring API keys for consuming the service.


3. An index file acting as a controller for orchesting different request made from end-users. 
   We have adopted a strategy for resuming app resquest from header file.


